FROM cern/alma9-base AS frontend
WORKDIR /oais-web
COPY ./oais-web .
RUN dnf module install -y nodejs:18
RUN npm install -g npm@10.7.0 && npm install --force && npm run build

FROM cern/alma9-base
ENV PYTHONUNBUFFERED=1
RUN dnf install -y epel-release && \
    dnf install -y dnf-plugins-core && \
    dnf install -y python3.11 python3.11-devel && \
    ln -sfn /usr/bin/python3.11 /usr/bin/python3 && \
    python3.11 -m ensurepip --upgrade && \
    dnf groupinstall -y "Development Tools" && \
    dnf install -y \
      cairo \
      cairo-devel \
      cargo \
      freetype-devel \
      gdk-pixbuf2 \
      gdk-pixbuf2-devel \
      gettext \
      glibc-devel \
      kernel-headers \
      krb5-devel \
      libjpeg-devel \
      lcms2-devel \
      libffi-devel \
      openjpeg2-devel \
      openssl-devel \
      pango \
      pango-devel \
      poppler-utils \
      postgresql \
      postgresql-devel \
      rust \
      tcl-devel \
      libtiff-devel \
      tk-devel \
      zlib \
      zlib-devel \
      git \
      swig 

WORKDIR /oais-platform
COPY ./oais-platform/docker/carepo.repo /etc/yum.repos.d/
RUN dnf install -y ca_CERN-Root-2 ca_CERN-GridCA && dnf clean -y all

COPY ./oais-platform/requirements.txt ./
RUN pip3 install -r requirements.txt --no-cache-dir
COPY ./oais-platform .
COPY --from=frontend /oais-web/build /assets
CMD ["sh", "-c", "python3 manage.py migrate && python3 manage.py runserver"]
