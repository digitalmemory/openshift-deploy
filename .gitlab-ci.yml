variables:
  GIT_SUBMODULE_STRATEGY: recursive
  IMAGE_NAME: ${CI_REGISTRY_IMAGE}/oais
  IS_DEVELOP: "false"

stages:
  - update
  - build
  - deploy

push-update:
  stage: update
  image: registry.cern.ch/docker.io/library/ubuntu
  # Only execute this job when the pipeline is triggered by the upstream repositories
  rules:
    - if: '$CI_PIPELINE_SOURCE == "pipeline"'
  script:
    - apt-get update
    - apt-get install -y git
    - cd /tmp
    - git clone --recurse-submodules "https://gitlab-ci:${GIT_ACCESS_TOKEN}@gitlab.cern.ch/digitalmemory/openshift-deploy.git"
    - cd openshift-deploy
    - git config user.name "GitLab CI"
    - git config user.email "ci@gitlab.cern.ch"
    - git submodule update --remote
    - git add .
    - git commit -am "Updating submodules" || true
    - git push

# Build and publish docker image using Kaniko.
# From https://gitlab.cern.ch/gitlabci-examples/build_docker_image/
build-docker-dev:
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
      changes: 
        - develop/**/*
  image:
    name: registry.cern.ch/gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "Commit branch is $CI_COMMIT_BRANCH, Context file is $CI_PROJECT_DIR/$CI_COMMIT_BRANCH and dockerfile is in $CI_PROJECT_DIR/Dockerfile"
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --cache=false --context $CI_PROJECT_DIR/develop --dockerfile $CI_PROJECT_DIR/Dockerfile --destination "${IMAGE_NAME}_dev":latest --destination "${IMAGE_NAME}_dev":$CI_COMMIT_SHA
    - echo "Image pushed to ${IMAGE_NAME}_dev:latest and ${IMAGE_NAME}_dev:$CI_COMMIT_SHA"

build-docker-qa:
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
      changes: 
        - qa/**/*
  image:
    name: registry.cern.ch/gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "Commit branch is $CI_COMMIT_BRANCH, Context file is $CI_PROJECT_DIR/$CI_COMMIT_BRANCH and dockerfile is in $CI_PROJECT_DIR/Dockerfile"
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --cache=false --context $CI_PROJECT_DIR/qa --dockerfile $CI_PROJECT_DIR/Dockerfile --destination "${IMAGE_NAME}_qa":latest --destination "${IMAGE_NAME}_qa":$CI_COMMIT_SHA
    - echo "Image pushed to ${IMAGE_NAME}_qa:latest and ${IMAGE_NAME}_qa:$CI_COMMIT_SHA"

build-docker-master:
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
      changes: 
        - master/**/*
  image:
    name: registry.cern.ch/gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "Commit branch is $CI_COMMIT_BRANCH, Context file is $CI_PROJECT_DIR/$CI_COMMIT_BRANCH and dockerfile is in $CI_PROJECT_DIR/Dockerfile"
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --cache=false --context $CI_PROJECT_DIR/master --dockerfile $CI_PROJECT_DIR/Dockerfile --destination "${IMAGE_NAME}_master":latest --destination "${IMAGE_NAME}_master":$CI_COMMIT_SHA
    - echo "Image pushed to ${IMAGE_NAME}_master:latest and ${IMAGE_NAME}_master:$CI_COMMIT_SHA"

deploy-dev:
  stage: deploy
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
      changes: 
        - develop/**/*
  image: registry.cern.ch/docker.io/library/ubuntu
  environment:
    name: luteus
    url: https://dm-luteus.web.cern.ch/
  script:
    - apt-get update
    - apt-get install -y curl
    # Download OpenShift client
    - curl https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.8.4/openshift-client-linux.tar.gz -o oc.tar.gz
    - tar -xf oc.tar.gz
    - mv ./oc /usr/bin/oc
    # Download Helm
    - curl https://get.helm.sh/helm-v3.6.3-linux-amd64.tar.gz -o helm.tar.gz
    - tar -xf helm.tar.gz
    - mv ./linux-amd64/helm /usr/bin/helm
    # Login
    - oc login --server="$DEV_OPENSHIFT_SERVER" --token="$DEV_OPENSHIFT_TOKEN"
    # Upgrade
    - oc project dm-luteus
    - >-
      helm upgrade -f ./develop/values.yaml "$DEV_HELM_NAME" ./oais-openshift --install
      --set oais.image="${IMAGE_NAME}_dev":latest

deploy-qa:
  stage: deploy
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
      changes: 
        - qa/**/*
  image: registry.cern.ch/docker.io/library/ubuntu
  environment:
    name: preserve-qa
    url: https://preserve-qa.web.cern.ch
  script:
    - apt-get update
    - apt-get install -y curl
    # Download OpenShift client
    - curl https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.8.4/openshift-client-linux.tar.gz -o oc.tar.gz
    - tar -xf oc.tar.gz
    - mv ./oc /usr/bin/oc
    # Download Helm
    - curl https://get.helm.sh/helm-v3.6.3-linux-amd64.tar.gz -o helm.tar.gz
    - tar -xf helm.tar.gz
    - mv ./linux-amd64/helm /usr/bin/helm
    # Login
    - oc login --server="$DEV_OPENSHIFT_SERVER" --token="$DEV_OPENSHIFT_TOKEN"
    # Upgrade
    - oc project preserve-qa
    - >-
      helm upgrade -f ./qa/values.yaml "$DEV_HELM_NAME" ./oais-openshift --install
      --set oais.image="${IMAGE_NAME}_qa":${CI_COMMIT_SHA}

deploy-master:
  stage: deploy
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
      changes: 
        - master/**/*
  image: registry.cern.ch/docker.io/library/ubuntu
  environment:
    name: galanos
    url: https://dm-galanos.web.cern.ch/
  script:
    - apt-get update
    - apt-get install -y curl
    # Download OpenShift client
    - curl https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.8.4/openshift-client-linux.tar.gz -o oc.tar.gz
    - tar -xf oc.tar.gz
    - mv ./oc /usr/bin/oc
    # Download Helm
    - curl https://get.helm.sh/helm-v3.6.3-linux-amd64.tar.gz -o helm.tar.gz
    - tar -xf helm.tar.gz
    - mv ./linux-amd64/helm /usr/bin/helm
    # Login
    - oc login --server="$DEV_OPENSHIFT_SERVER" --token="$DEV_OPENSHIFT_TOKEN"
    # Upgrade
    - oc project dm-galanos
    - >-
      helm upgrade -f ./master/values.yaml "$DEV_HELM_NAME" ./oais-openshift --install
      --set oais.image="${IMAGE_NAME}_master":${CI_COMMIT_SHA}

build-docker-mr-test:
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
  image:
    name: registry.cern.ch/gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "Commit branch is $CI_COMMIT_BRANCH, dockerfile is in $CI_PROJECT_DIR/Dockerfile"
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --cache=false --context $CI_PROJECT_DIR/develop --dockerfile $CI_PROJECT_DIR/Dockerfile --destination "${IMAGE_NAME}_dev":test --destination "${IMAGE_NAME}_dev":$CI_COMMIT_SHA
    - echo "Image pushed to ${IMAGE_NAME}_dev:test and ${IMAGE_NAME}_dev:$CI_COMMIT_SHA"
